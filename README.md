# AdsCode API Rest

## Description

API REST para la aplicación AdsCode - lector de códigos de publicidad donde se introduce un código y se muestra el contenido asociado a ese código.

## Installation

Clone the repository and run the following commands under your project root:

```shell
npm install
npm start
```