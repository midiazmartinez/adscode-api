"use strict";

/**
 * UserRoles
 * @description :: Model for storing UserRoles records
 */

module.exports = {
  schema: true,

  tableName: 'users_roles',

  attributes: {

    users:{
      model:'user'
    },

    roles: {
      model: 'role'
    },

    toJSON() {
      return this.toObject();
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
