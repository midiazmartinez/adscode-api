'use strict';

/**
 * isSuper
 * @description :: Policy that inject user in `req` via JSON Web Token and check is role is super
 */

const passport = require('passport');

module.exports = (req, res, next) => {
  passport.authenticate('jwt', (error, user, info) => {
    if (error || !user) return res.negotiate(error || info);
    if (user.maxRole > 1) return res.negotiate({ status: 403 });
    req.user = user;
    next();
  })(req, res);
};
