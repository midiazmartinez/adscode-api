'use strict';

/**
 * Connections API Configuration
 *
 * Connections are like "saved settings" for your adapters.
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 *
 * NOTE: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 */

module.exports = {
  connections: {
    /**
     * MySQL configuration
     * @type {Object}
     */
    mysql: {
      adapter: 'sails-mysql',
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: '1a2b3c4d',
      database: 'adscode'
    },

    /**
     * MySQL configuration PROD
     * @type {Object}
     */
    mysql_prod: {
      adapter: 'sails-mysql',
      host: 'eu-cdbr-west-01.cleardb.com',
      port: 3306,
      user: 'b5ba4c44286792',
      password: '8584e71e',
      database: 'heroku_e6ce0c9785c58e1'
    }
  }
};
