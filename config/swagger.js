'use strict';

module.exports = {
  swagger: {
    pkg: {
      name: 'Cod2Info API',
      description: 'API REST para la app [`Cod2Info`](http://cod2.info/) un lector de códigos de publicidad donde se introduce un código y se muestra el contenido asociado a ese código.',
      basePath: '/v1',
      version: '0.1.0',
      author: 'Cod2Info',
      homepage: 'About',
      license: ''
    }
  }
};
